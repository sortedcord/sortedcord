<!-- <style>
td, th {
   border: none!important;
}
</style> -->

<img src="vid.gif" />


<h1>My Weapons</h1>

<h3>Frontend</h3>

<img src="https://img.shields.io/badge/HTML5-E34F26?style=flat&logo=html5&logoColor=white" /> <img src="https://img.shields.io/badge/CSS3-1572B6?style=flat&logo=css3&logoColor=white" /> <img src="https://img.shields.io/badge/JavaScript-F7DF1E?style=flat&logo=javascript&logoColor=black" /> <img src="https://img.shields.io/badge/Vue.js-35495E?style=flat&logo=vuedotjs&logoColor=4FC08D" /> <img src="https://img.shields.io/badge/Tailwind_CSS-38B2AC?style=flat&logo=tailwind-css&logoColor=white" /> <img src="https://img.shields.io/badge/Flutter-02569B?style=flat&logo=flutter&logoColor=white" /> <img src="https://img.shields.io/badge/Figma-F24E1E?style=flat&logo=figma&logoColor=white" /> <img src="https://img.shields.io/badge/Adobe%20XD-470137?style=flat&logo=Adobe%20XD&logoColor=#FF61F6" /> <img src="https://img.shields.io/badge/Canva-%2300C4CC.svg?&style=flat&logo=Canva&logoColor=white" />

<h3>Backend</h3>

<img src="https://img.shields.io/badge/Python-FFD43B?style=flat&logo=python&logoColor=darkgreen" /> <img src="https://img.shields.io/badge/Django-092E20?style=flat&logo=django&logoColor=green" /> <img src="https://img.shields.io/badge/MariaDB-003545?style=flat&logo=mariadb&logoColor=white" /> <img src="https://img.shields.io/badge/PHP-777BB4?style=flat&logo=php&logoColor=white" />

<h3>DevOPs</h3>

<img src="https://img.shields.io/badge/Nginx-009639?style=flat&logo=nginx&logoColor=white" /> <img src="https://img.shields.io/badge/Docker-2CA5E0?style=flat&logo=docker&logoColor=white" />

<h3>Operating Systems</h3>

<img src="https://img.shields.io/badge/Ubuntu-E95420?style=flat&logo=ubuntu&logoColor=white" /> <img src="https://img.shields.io/badge/Arch_Linux-1793D1?style=flat&logo=arch-linux&logoColor=white" /> <img src="https://img.shields.io/badge/Debian-A81D33?style=flat&logo=debian&logoColor=white" /> <img src="https://img.shields.io/badge/manjaro-35BF5C?style=flat&logo=manjaro&logoColor=white" /> <img src="https://img.shields.io/badge/Windows-0078D6?style=flat&logo=windows&logoColor=white" /> <img src="https://img.shields.io/badge/mac%20os-000000?style=flat&logo=apple&logoColor=white" /> <img src="https://img.shields.io/badge/lineageos-167C80?style=flat&logo=lineageos&logoColor=white" />



<h1>Metrics</h1>

\*NOTE: Top languages does not indicate my skill level or something like that, it's a github metric of which languages i have the most code on github.

<p align="center">
<img src="https://github-readme-stats-sortedcord.vercel.app/api/top-langs/?username=sortedcord&layout=compact&theme=algolia&hide=html&langs_count=8&hide_border=true" width="40.9%"/> 
<img width="56.9%" src="http://github-readme-stats-sortedcord.vercel.app/api?username=sortedcord&show_icons=true&theme=algolia&hide_border=true&include_all_commits=true&count_private=true"  />

<img width="47.9%" src="http://github-readme-streak-stats.herokuapp.com?user=sortedcord&theme=algolia&hide_border=true" />
<img width="49.9%" src="https://github-readme-stats.vercel.app/api/wakatime?username=sortedcord&layout=compact&theme=algolia&hide_border=true&langs_count=8" />
</p>

<details>
<summary><b>📊 Github Contribution Graph</b></summary>
<br>
<p align="center"<a href="#"><img alt="sortedcord's Activity Graph" src="https://activity-graph.herokuapp.com/graph?username=sortedcord&theme=react-dark" /></a></p>
</details>


<h1>🏆 Trophies</h1>
<p align="center"> <a href="https://github.com/sortedcord"><img width="97.8%" src="https://github-profile-trophy.vercel.app/?username=sortedcord&theme=algolia&column=7&margin-w=5&no-frame=true" alt="sortedcord" /></a> </p>

<br>

<h2>🎨 Dot Repos</h2>

<p align="center">
  <a href="https://github.com/sortedcord/Gruvbox-Pink-Dots"><img width="48.9%" src="https://github-readme-stats-sortedcord.vercel.app/api/pin/?username=sortedcord&repo=Gruvbox-Pink-Dots&theme=algolia&hide_border=true" alt="Gruvbox-Pink-Dots"></a>
  <a href="https://github.com/sortedcord/Sortify"><img width="48.9%" src="https://github-readme-stats-sortedcord.vercel.app/api/pin/?username=sortedcord&repo=Sortify&theme=algolia&hide_border=true" alt="sortify"></a>
    <a href="https://github.com/sortedcord/sweet-mars-i3"><img width="48.9%" src="https://github-readme-stats-sortedcord.vercel.app/api/pin/?username=sortedcord&repo=sweet-mars-i3&theme=algolia&hide_border=true" alt="Sweet mars i3"></a>
  <a href="https://github.com/sortedcord/Dracula"><img width="48.9%" src="https://github-readme-stats-sortedcord.vercel.app/api/pin/?username=sortedcord&repo=Dracula&theme=algolia&hide_border=true" alt="Dracula"></a>
</p>

<br>

<h2>📘 Notes Repos</h2>

<p align="center">
  <a href="https://github.com/sortedcord/vue-notes"><img width="48.9%" src="https://github-readme-stats-sortedcord.vercel.app/api/pin/?username=sortedcord&repo=vue-notes&theme=algolia&hide_border=true" alt="vue-notes"></a>
  <a href="https://github.com/sortedcord/angular-notes"><img width="48.9%" src="https://github-readme-stats-sortedcord.vercel.app/api/pin/?username=sortedcord&repo=angular-notes&theme=algolia&hide_border=true" alt="sortify"></a>
</p>
